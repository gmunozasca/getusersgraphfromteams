export const msalConfig = {
    auth: {
      clientId: "67e2fe7f-6877-4544-978f-1bd10ce5248d",
      authority: "https://login.microsoftonline.com/2d6e1f01-b6e6-43a6-bbbb-8faf3432292a", // This is a URL (e.g. https://login.microsoftonline.com/{your tenant ID})
      redirectUri: "https://localhost:53000/test",
    },
    cache: {
      cacheLocation: "sessionStorage", // This configures where your cache will be stored
      storeAuthStateInCookie: false, // Set this to "true" if you are having issues on IE11 or Edge
    }
  };
  
  // Add scopes here for ID token to be used at Microsoft identity platform endpoints.
  export const loginRequest = {
   scopes: ["Directory.Read.All"]
  };
  
  // Add the endpoints here for Microsoft Graph API services you'd like to use.
  export const graphConfig = {
      graphUsersEndpoint: "https://graph.microsoft.com/v1.0/users"
  };