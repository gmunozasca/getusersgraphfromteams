import { useState } from "react";
import "./Welcome.css";

import { useIsAuthenticated } from "@azure/msal-react";
import { SignInButton } from "../SignInButton";
import { SignOutButton } from "../SignOutButton";
import { callMsGraph } from "./UsersGraph";

function UserList(props: any) {
  const users = props.users;
  const listItems = users.map((user) =>
    <li key={user.toString()}>
      {user}
    </li>
  );
  return (
    <ul>{listItems}</ul>
  );
}

export function Welcome(props: { showFunction?: boolean; environment?: string }) {
  const isAuthenticated = useIsAuthenticated();

  const [users, setUsers] = useState([])

  function modifyUsers(token) {
    callMsGraph(token).then(response => {
        setUsers(response.value.map((user) => user.displayName + ' ' + user.mail))
    });
  }

  return (
    <div className="welcome page">
      <div className="narrow page-padding">
        <h1 className="center">Prueba de lista de usuarios con Microsoft Graph</h1>
        { isAuthenticated ? <SignOutButton /> : <SignInButton updateUsers={modifyUsers} /> }
        <UserList users={users} />
      </div>
    </div>
  );
}
