import { useMsal } from "@azure/msal-react";
import { loginRequest } from "../authConfig";
import { Button } from "@fluentui/react-northstar";

function handleLogin(instance: any, updateUsers: any) {
    instance.loginPopup(loginRequest).then(value => {
        updateUsers(value.accessToken)
    })
    .catch(e => {
        console.error(e);
    });
}

/**
 * Renders a button which, when selected, will open a popup for login
 */
export const SignInButton = (props: { updateUsers?: any }) => {
    const { instance } = useMsal();

    return (
        <Button secondary content="Sign in using Popup" onClick={() => handleLogin(instance, props.updateUsers)} />
    );
}