import { useMsal } from "@azure/msal-react";
import { Button } from "@fluentui/react-northstar";

function handleLogout(instance: any) {
    instance.logoutPopup().catch(e => {
        console.error(e);
    });
}

/**
 * Renders a button which, when selected, will open a popup for logout
 */
export const SignOutButton = () => {
    const { instance } = useMsal();

    return (
        <Button secondary content="Cerrar sesión" onClick={() => handleLogout(instance)} />
    );
}